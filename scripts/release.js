#!/usr/bin/env node

import chalk from 'chalk';
import { execPromisify, readFileContent, resolveApp } from '@baryman/utils';

try {
  const prevVersion = JSON.parse(
    readFileContent(resolveApp('package.json'))
  ).version;
  await execPromisify('yarn config set version-commit-hooks false');
  await execPromisify(`yarn version --patch`);
  const currentVersion = JSON.parse(
    readFileContent(resolveApp('package.json'))
  ).version;
  await execPromisify(`yarn publish --access public`);
  await execPromisify('yarn config set version-commit-hooks true');
  console.log('Publish...');
  console.log('Previous version:', chalk.yellow(prevVersion));
  console.info('Current version:', chalk.cyan(currentVersion));
  console.log('Status:', chalk.green('Published'));
  process.exit(0);
} catch (error) {
  console.error('\x1b[31m%s\x1b[0m', error);
  process.exit(1);
}
