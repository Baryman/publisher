#!/usr/bin/env node

import chalk from 'chalk';
import { execPromisify, readFileContent, resolveApp } from '@baryman/utils';

try {
  const prevVersion = JSON.parse(
    readFileContent(resolveApp('package.json'))
  ).version;
  await execPromisify('yarn config set version-commit-hooks false');
  await execPromisify(`yarn version --prerelease --preid beta`);
  const currentVersion = JSON.parse(
    readFileContent(resolveApp('package.json'))
  ).version;
  await execPromisify(`yarn publish --tag beta`);
  await execPromisify('yarn config set version-commit-hooks true');
  console.log('Publish beta...');
  console.log('Previous version:', chalk.yellow(prevVersion));
  console.info('Current version:', chalk.cyan(currentVersion));
  console.log('Status:', chalk.green('Published'));
  process.exit(0);
} catch (error) {
  console.error('\x1b[31m%s\x1b[0m', error);
  process.exit(1);
}
