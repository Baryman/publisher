# Do Release with Publisher

## Get started

### NPM
`$ npm install --save-dev @baryman/publisher`

### Yarn

`$ yarn add -D @baryman/publisher`

## Usage

`$ relese`

Release your package to npmjs and mutation patch version

`$ relese-beta`

Release your package to npmjs and mutation version with tag beta